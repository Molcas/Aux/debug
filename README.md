Debug jobs in a runner by setting the `IMAGE`, `TAG` and `SCRIPT` CI variables.

Useful links:  
https://tunshell.com/go  
https://paste.c-net.org/
